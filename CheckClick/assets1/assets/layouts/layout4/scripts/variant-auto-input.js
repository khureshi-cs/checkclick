﻿var colorDiv = $("#color0")[0].outerHTML;
bindColorTextboxes();
removeFirstElement();

function bindColorTextboxes() {
    $(".txtColorEn").unbind("input propertychange paste");
    $(".txtColorAr").unbind("input propertychange paste");
    $(".txtColorEn").on("input propertychange paste", function () {
        addDynamicColor($(this).attr("id"));
    });
    $(".txtColorAr").on("input propertychange paste", function () {
        addDynamicColor($(this).attr("id"));
    });
    $(".removeColor").on("click", function () {
        removeDynamicColor($(this).attr("id"));
    });
}

function addDynamicColor(controlId) {
    var currIndex = 0;
    if (controlId.indexOf("txtColorEn") >= 0) {
        currIndex = parseInt(controlId.replace("txtColorEn", ""));
    }
    else if (controlId.indexOf("txtColorAr") >= 0) {
        currIndex = parseInt(controlId.replace("txtColorAr", ""));
    }
    if ($("#color" + (currIndex + 1)).length == 0) {
        var newIndex = (currIndex + 1);
        colorDivCurr = colorDiv.replace("color0", "color" + newIndex).replace("txtColorEn0", "txtColorEn" + newIndex)
            .replace("txtColorAr0", "txtColorAr" + newIndex).replace("btnRemoveColor0", "btnRemoveColor" + newIndex);
        $("#color" + currIndex).after(colorDivCurr);
        bindColorTextboxes();
    }
    removeFirstElement();
}

function removeDynamicColor(controlId) {
    var currIndex = -1;
    if (controlId.indexOf("btnRemoveColor") >= 0) {
        currIndex = parseInt(controlId.replace("btnRemoveColor", ""));
    }
    if ($("#color" + currIndex).length > 0) {
        $("#color" + currIndex).remove();
    }
    removeFirstElement();
}
function removeFirstElement() {
    if ($(".divColor").length == 1) {
        $(".divColor").first().find($(".removeColor")).addClass("hidden");
    }
    else {
        $(".divColor").first().find($(".removeColor")).removeClass("hidden");
    }
}

var divSize = $("#divSize0")[0].outerHTML;
bindSizeTextbox();
removeFirstElementSize();

function bindSizeTextbox() {
    $(".txtSize").unbind("input propertychange paste");
    $(".txtSize").on("input propertychange paste", function () {
        addDynamicSize($(this).attr("id"));
    });
    $(".btnRemoveSize").on("click", function () {
        removeDynamicSize($(this).attr("id"));
    });
}

function addDynamicSize(controlId) {
    var currIndex = -1;
    if (controlId.indexOf("txtSize") >= 0) {
        currIndex = parseInt(controlId.replace("txtSize", ""));
    }

    if ($("#divSize" + (currIndex + 1)).length == 0) {
        var newIndex = (currIndex + 1);
        divSizeCurr = divSize.replace("divSize0", "divSize" + newIndex).replace("txtSize0", "txtSize" + newIndex).replace("btnRemoveSize0", "btnRemoveSize" + newIndex);
        $("#divSize" + currIndex).after(divSizeCurr);
        bindSizeTextbox();
    }
    removeFirstElementSize();
}

function removeDynamicSize(controlId) {
    var currIndex = -1;
    if (controlId.indexOf("btnRemoveSize") >= 0) {
        currIndex = parseInt(controlId.replace("btnRemoveSize", ""));
    }
    if ($("#divSize" + currIndex).length > 0) {
        $("#divSize" + currIndex).remove();
    }
    removeFirstElementSize();
}
function removeFirstElementSize() {
    console.log($(".divSize").length);
    if ($(".divSize").length == 1) {
        $(".divSize").first().find($(".btnRemoveSize")).addClass("hidden");
    }
    else {
        $(".divSize").first().find($(".btnRemoveSize")).removeClass("hidden");
    }
}
