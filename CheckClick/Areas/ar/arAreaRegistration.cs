﻿using System.Web.Mvc;

namespace CheckClick.Areas.ar
{
    public class arAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ar";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ar_default",
                "ar/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}