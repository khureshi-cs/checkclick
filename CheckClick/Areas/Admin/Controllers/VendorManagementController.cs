﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckClick.Areas.Admin.Controllers
{
    public class VendorManagementController : Controller
    {
        // GET: Admin/VendorManagement
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateStore()
        {
            return View();
        }

        public ActionResult CreateBranch()
        {
            return View();
        }

        public ActionResult VendorProducts()
        {
            return View();
        }

        public ActionResult StoreProducts()
        {
            return View();
        }

        public ActionResult VendorSubscriptions()
        {
            return View();
        }

        public ActionResult VendorSubscriptionRenew()
        {
            return View();
        }

        public ActionResult VendorDetails()
        {
            return View();
        }
        public ActionResult VendorOrders()
        {
            return View();
        }



    }
}