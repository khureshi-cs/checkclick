﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckClick.Areas.Admin.Controllers
{
    public class StoreRequestsController : Controller
    {
        // GET: Admin/StoreRequests
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PendingRequest()
        {
            return View();
        }
        public ActionResult ApprovedRequest()
        {
            return View();
        }
        public ActionResult RejectedRequest()
        {
            return View();
        }

        public ActionResult Store()
        {
            return View();
        }
    }
}