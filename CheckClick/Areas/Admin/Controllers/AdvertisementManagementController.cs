﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckClick.Areas.Admin.Controllers
{
    public class AdvertisementManagementController : Controller
    {
        // GET: Admin/AdvertisementManagement
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
        public ActionResult AdvertisementDetails()
        {
            return View();
        }

        public ActionResult AllBanners()
        {
            return View();
        }
        public ActionResult AllFutureBanners()
        {
            return View();
        }

        public ActionResult AllExpiredBanners()
        {
            return View();
        }
    }
}