﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckClick.Areas.Vendor
{
    public class OrderManagementController : Controller
    {
        // GET: OrderManagement
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Branch()
        {
            return View();
        }

        public ActionResult Orders()
        {
            return View();
        }
        public ActionResult OrderInfo()
        {
            return View();
        }
        public ActionResult ScheduledOrders()
        {
            return View();
        }
        public ActionResult SchedluedOrderInfo()
        {
            return View();
        }
        public ActionResult TransferOrders()
        {
            return View();
        }
        public ActionResult TransferOrderInfo()
        {
            return View();
        }

        public ActionResult DelayedOrders()
        {
            return View();
        }

        public ActionResult DelayedOrderInfo()
        {
            return View();
        }
        public ActionResult ReturnedOrders()
        {
            return View();
        }

        public ActionResult ReturnOrderInfo()
        {
            return View();
        }
        public ActionResult ReturnedOrderProcess()
        {
            return View();
        }

        public ActionResult CancelOrders()
        {
            return View();
        }

        public ActionResult CancelOrderInfo()
        {
            return View();
        }

        public ActionResult History()
        {
            return View();
        }
    }
}