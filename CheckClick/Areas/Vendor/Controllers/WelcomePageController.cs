﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckClick.Areas.Vendor
{
    public class WelcomePageController : Controller
    {
        // GET: StoreDashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}