﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckClick.Areas.Vendor
{
    public class AccessManagementController : Controller
    {
        // GET: AccessManagement
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult UsersList()
        {
            return View();
        }

        public ActionResult UserAccess()
        {
            return View();
        }

        public ActionResult EditUserAccess()
        {
            return View();
        }
    }
}