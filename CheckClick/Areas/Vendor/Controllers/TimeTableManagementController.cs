﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CheckClick.Areas.Vendor
{
    public class TimeTableManagementController : Controller
    {
        // GET: TimeTableManagement
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AssignProduct()
        {
            return View();
        }
        public ActionResult AssignService()
        {
            return View();
        }

        public ActionResult TimeSlots()
        {
            return View();
        }
        public ActionResult TimeSlotsService()
        {
            return View();
        }
        public ActionResult ServiceProvider()
        {
            return View();
        }
    }
}